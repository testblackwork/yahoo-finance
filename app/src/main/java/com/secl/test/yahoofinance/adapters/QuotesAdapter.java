package com.secl.test.yahoofinance.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.secl.test.yahoofinance.R;
import com.secl.test.yahoofinance.models.Quote;

import java.util.ArrayList;
import java.util.List;

public class QuotesAdapter extends BaseAdapter {
    private List<Quote> mItems;
    private LayoutInflater mLayoutInflater;
    private boolean showImage = true;

    public QuotesAdapter(Context context, ArrayList<Quote> mItems) {
        if (null == mItems) {
            this.mItems = new ArrayList<Quote>();
        } else {
            this.mItems = mItems;
        }

        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.list_item, null);
            holder = new ViewHolder();
            holder.mSymbol = (TextView) convertView.findViewById(R.id.symbol);
            holder.mAsk = (TextView) convertView.findViewById(R.id.ask);
            holder.mChange = (TextView) convertView.findViewById(R.id.change);
            holder.mPrev = (TextView) convertView.findViewById(R.id.prev);
            holder.mLow = (TextView) convertView.findViewById(R.id.low);
            holder.mHigh = (TextView) convertView.findViewById(R.id.high);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Quote item = mItems.get(position);
        holder.mSymbol.setText(item.getSymbol());
        holder.mAsk.setText(item.getAsk());
        holder.mChange.setText(item.getChange());
        holder.mPrev.setText(item.getPrevClose());
        holder.mLow.setText(item.getDaysLow());
        holder.mHigh.setText(item.getDaysHigh());

        return convertView;
    }

    public void refreshList(List<Quote> list){
        if (null == list){
            list = new ArrayList<Quote>();
        }
        mItems.clear();
        mItems.addAll(list);
        notifyDataSetChanged();
    }

    static class ViewHolder {
        TextView mSymbol;
        TextView mAsk;
        TextView mChange;
        TextView mPrev;
        TextView mLow;
        TextView mHigh;
    }
}
