package com.secl.test.yahoofinance.helpers;

import android.util.Log;
import com.secl.test.yahoofinance.models.Quote;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DataHelper {

    public static List<Quote> getDataFromJsonStr(String jsonStr){
        List<Quote> result = new ArrayList<Quote>();
        try {
            JSONObject dataJsonObj = new JSONObject(jsonStr);
            JSONObject queryObj = dataJsonObj.getJSONObject("query");
            JSONObject resultsObj = queryObj.getJSONObject("results");

            if (null != resultsObj.optJSONArray("quote")){
                JSONArray quoteArray = resultsObj.getJSONArray("quote");
                for (int i = 0; i < quoteArray.length(); i++) {
                    JSONObject quoteObj = quoteArray.getJSONObject(i);
                    Quote quote = new Quote(quoteObj);
                    result.add(quote);
                }
            } else {
                Quote quote = new Quote(resultsObj.getJSONObject("quote"));
                result.add(quote);
            }
        } catch (JSONException e) {
            Log.d(DataHelper.class.getCanonicalName(), e.toString());
        }

        return result;
    }
}
