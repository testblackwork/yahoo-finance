package com.secl.test.yahoofinance.network;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class RequestService {
    private static final String CORE_URL = "https://query.yahooapis.com/v1/" +
            "public/yql?q=select%20*%20from%20" +
            "yahoo.finance.quotes";

    private static final String END_OF_URL= "&format=json&env=store%3A%2F%2Fdatatables.org" +
            "%2Falltableswithkeys&callback=";

    public static boolean getFinanceDataByCodes(String[] codes, ResultHandler handler){
        String params = "%20where%20symbol%20in%20(";
        for (int i = 0; i < codes.length; i++) {
            params += "%22" + codes[i] + "%22";
            if (i < codes.length - 1){
                params += "%2C";
            }
        }
        params += ")";
        return  callService(CORE_URL + params + END_OF_URL, handler);
    }

    public static boolean callService(String url, ResultHandler handler) {
        try {
            URL destination = new URL(url);
            HttpURLConnection con = (HttpURLConnection) destination.openConnection();

            String readStream = readStream(con.getInputStream());

            if (handler != null)
                return handler.handleResult(readStream);
            else
                return true;
        } catch (Exception e) {
            Log.d(RequestService.class.getCanonicalName(), e.toString());
            return false;
        }
    }

    private static String readStream(InputStream in) {
        StringBuilder sb = new StringBuilder();
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String nextLine = "";
            while ((nextLine = reader.readLine()) != null) {
                sb.append(nextLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
