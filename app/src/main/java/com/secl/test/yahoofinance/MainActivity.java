package com.secl.test.yahoofinance;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.secl.test.yahoofinance.adapters.QuotesAdapter;
import com.secl.test.yahoofinance.config.Config;
import com.secl.test.yahoofinance.helpers.DataHelper;
import com.secl.test.yahoofinance.models.Quote;
import com.secl.test.yahoofinance.network.RequestService;
import com.secl.test.yahoofinance.network.ResultHandler;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private QuotesAdapter mAdapter;
    private Toast mToast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ListView list = (ListView) findViewById(R.id.list);
        mAdapter = new QuotesAdapter(this, null);
        View header = getLayoutInflater().inflate(R.layout.list_header, list, false);
        list.addHeaderView(header, null, false);
        list.setAdapter(mAdapter);

        mToast = Toast.makeText(this, "", Toast.LENGTH_LONG);
        mToast.setGravity(Gravity.CENTER, 0 , 0);

        new LoadDataTask().execute(Config.DEFAULT_QUOTES);
    }

    @Override
    protected void onResume(){
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                new LoadDataTask().execute(new String[] { query });
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_refresh) {
            new LoadDataTask().execute(Config.DEFAULT_QUOTES);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class LoadDataTask extends AsyncTask<String[], Void, Boolean> {
        private final ProgressDialog mProgressDialog = new ProgressDialog(MainActivity.this);
        private List<Quote> items;

        protected void onPreExecute() {
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.show();
        }

        protected Boolean doInBackground(final String[]... args) {
            boolean result = false;

            try {
                ResultHandler resultHandler = new ResultHandler() {
                    @Override
                    public boolean handleResult(String response) {
                        boolean result = true;
                        items = DataHelper.getDataFromJsonStr(response);
                        return result;
                    }
                };

                if (RequestService.getFinanceDataByCodes(args[0], resultHandler)){
                    result = true;
                }
            }catch (Exception e){
                Log.v(MainActivity.class.getCanonicalName(), e.toString());
            }

            return result;
        }

        protected void onPostExecute(final Boolean success) {
            Context context = MainActivity.this;
            if (success) {
                if (0 == items.size()){
                    mToast.setText(context.getString(R.string.quote_not_found));
                    mToast.show();
                } else {
                    mAdapter.refreshList(items);
                }
            } else {
                mToast.setText(context.getString(R.string.load_data_error));
                mToast.show();
            }

            if (mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        }
    }
}
