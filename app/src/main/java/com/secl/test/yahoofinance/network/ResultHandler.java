package com.secl.test.yahoofinance.network;

public interface ResultHandler {
    boolean handleResult(String res);
}
