package com.secl.test.yahoofinance.models;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class Quote {
    private String symbol;
    private String prevClose;
    private String change;
    private String daysLow;
    private String daysHigh;
    private String ask;

    public void setAsk(String ask) {
        this.ask = ask;
    }

    public void setDaysHigh(String daysHigh) {
        this.daysHigh = daysHigh;
    }

    public void setDaysLow(String daysLow) {
        this.daysLow = daysLow;
    }

    public void setChange(String change) {
        this.change = change;
    }

    public void setPrevClose(String prevClose) {
        this.prevClose = prevClose;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getAsk() {
        return ask.equals("null") ? "-" : ask;
    }

    public String getDaysHigh() {
        return daysHigh.equals("null") ? "-" : daysHigh;
    }

    public String getDaysLow() {
        return daysLow.equals("null") ? "-" : daysLow;
    }

    public String getChange() {
        return change.equals("null") ? "-" : change;
    }

    public String getPrevClose() {
        return prevClose.equals("null") ? "-" : prevClose;
    }

    public String getSymbol() {
        return symbol;
    }

    public Quote(JSONObject jsonObj){
        try{
            symbol = jsonObj.getString("symbol");
            change = jsonObj.getString("Change");
            daysLow = jsonObj.getString("DaysLow");
            daysHigh = jsonObj.getString("DaysHigh");
            ask = jsonObj.getString("Ask");
            prevClose = jsonObj.getString("PreviousClose");
        } catch (JSONException e){
            Log.v(getClass().getCanonicalName(), e.toString());
        }
    }
}
